public class aula4 {

    public static void main(String[] args) {

        int num = 0;
        String s = "aaa";

        /* variavel:
        1) um nome
        2) um tipo
        3) um tamanho
        4) um valor
         */

        num = 10;

        /*
        1) armazenamento da memória é temporário
        2) os dados são removidos da memória quando o pc é desligado
        3) a memória é um tipo de armazenamento volatil
        4) o valor antigo sempre é perdido
        5) o java desaloca memória
         */
    }
}
